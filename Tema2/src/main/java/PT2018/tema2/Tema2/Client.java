package PT2018.tema2.Tema2;

public class Client {

	private int clientId;
	private int arrivalTime;
	private int processingTime;
	private int endServiceTime = 0;
	private int startServingTime = 0;
	
	Client(int clientId, int arrivalTime, int processingTime){
	
		this.arrivalTime = arrivalTime;
		this.clientId = clientId;
		this.processingTime = processingTime;
	}
	
	Client(){
		
		this( 0, 0, 0);
	}
	
	public int getProcessingTime() {
		
		return processingTime;
	}
	
	public int getArrivalTime() {
		
		return arrivalTime;
	}
	
	public int getClientId() {
		
		return clientId;
	}
	
	public int getEndServiceTime() {
		
		return endServiceTime;
	}
	
	public void setEndServiceTime(int endServiceTime) {
		
		this.endServiceTime = endServiceTime;
	}
	
	public int getStartServingTime() {
		
		return startServingTime;
	}
	
	public void setStartServingTime(int startServingTime) {
		
		this.startServingTime = startServingTime;
	}
}
