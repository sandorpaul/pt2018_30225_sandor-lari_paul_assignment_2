package PT2018.tema2.Tema2;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Queue implements Runnable{

	private BlockingQueue<Client> coada;
	
	private int queueNo;
	private int queueWaitingTime;
	private int lastServiceTime;
	private boolean isEmpty;
	
	private static int currentTime;
	private static int minimumIntervalOfArriving;
	private static int maximumIntervalOfArriving;
	private static int minimumServiceTime;
	private static int maximumServiceTime;
	private static int simulationInterval;
	
	
	Queue(int queueNo, int minimumIntervalOfArriving, int maximumIntervalOfArriving,
			int minimumServiceTime, int maximumServiceTime, int simulationInterval){
	
		this.coada = new LinkedBlockingQueue<Client>();
		this.queueNo = queueNo;
		this.queueWaitingTime = 0;
		this.lastServiceTime = 1;
		this.isEmpty = true;
		
		Queue.minimumIntervalOfArriving = minimumIntervalOfArriving;
		Queue.maximumIntervalOfArriving = maximumIntervalOfArriving;
		Queue.minimumServiceTime = minimumServiceTime;
		Queue.maximumServiceTime = maximumServiceTime;
		Queue.simulationInterval = simulationInterval;
		
	}
	
	public void addClientToQueue(Client newClient) {
		
		Client c = newClient;
		
		c.setStartServingTime(lastServiceTime);
		
		this.lastServiceTime += c.getProcessingTime();
		this.queueWaitingTime += lastServiceTime - currentTime;
		
		c.setEndServiceTime(lastServiceTime);
		
		coada.add(c);
		
		isEmpty = false;
	}

	public int getQueueSize() {
		
		return coada.size();
	}
	
	public void run() {
		
		//System.out.println("Q" + queueNo + " : " + currentTime + " " + simulationInterval);
		
			while(!coada.isEmpty() || currentTime <= simulationInterval) {
				//System.out.println("Q" + queueNo + " : " + currentTime + " " + simulationInterval);
				if(coada.size() > 0) {
					
					//System.out.println();
					//System.out.print("Queue" + queueNo + ": CT" + currentTime + " ");
					
					for(Client i : coada) {
						
						//System.out.println("Queue" + queueNo + ": CT" + currentTime + " Client" + i.getClientId() + " AT " + i.getArrivalTime() + " LS" + i.getEndServiceTime());
					}
					
					Client client = coada.peek();
					
					if(client.getEndServiceTime() <= currentTime) {
						coada.poll();
						queueWaitingTime -= client.getProcessingTime();
					}
					
					try {
							
						Thread.sleep(Scheduler.unitateTimp);
						
							
					}catch(InterruptedException e){
							
						e.printStackTrace();
					}
				}
				
				
				else { 
					//System.out.println("Queue " + queueNo+ " NUNUNUNUN " + currentTime);
					
					lastServiceTime = currentTime + 1;
					queueWaitingTime = 0;
					isEmpty = true;	
				}
				
				if(currentTime > simulationInterval)
					break;
		}
	}

	public BlockingQueue<Client> getClientsQueue(){

		return coada;
	}
	
	public boolean getIsEmpty() {
		
		return isEmpty;
	}
	
	public int getLastServiceTime() {
		
		return lastServiceTime;
	}
	
	public int getWatingTime() {
		
		return queueWaitingTime;
	}
	
	public static void setCurrentTime(int currentTime) {
		
		Queue.currentTime = currentTime;
	}

}