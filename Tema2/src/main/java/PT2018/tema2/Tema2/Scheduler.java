package PT2018.tema2.Tema2;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Scheduler implements Runnable{
	
	public static final int unitateTimp  = 1000;
	
	private Queue[] queues;
	private LinkedList<Client> waitingClients ;
	
	private int queueNumber;
	private int minimumIntervalOfArriving;
	private int maximumIntervalOfArriving;
	private int minimumServiceTime;
	private int maximumServiceTime;
	private int simulationInterval;
	private int currentTime;
	private boolean queuesOver;
	
	private int mediumWaitingTime;
	private int maxWaitingTime = 0;
	private int oraVarf = 0;
	
	private AfisareSimulare as;
	
	Scheduler(int queueNumber, LinkedList<Client> waitingClients, int minimumIntervalOfArriving, 
			int maximumIntervalOfArriving, int minimumServiceTime, int maximumServiceTime, 
			int simulationInterval){
		
		this.queueNumber = queueNumber;
		queues = new Queue[queueNumber];
		this.waitingClients = waitingClients;
		
		this.minimumIntervalOfArriving = minimumIntervalOfArriving;
		this.maximumIntervalOfArriving = maximumIntervalOfArriving;
		this.minimumServiceTime = minimumServiceTime;
		this.maximumServiceTime = maximumServiceTime;
		this.simulationInterval = simulationInterval;
		
		queuesOver = false;
		
		this.as = new AfisareSimulare(queueNumber);
	}
	
	public void insertClient() {
		
		Client waitingClient;
		
		if((waitingClient = waitingClients.pollFirst()) != null) {
			
			int min = queues[0].getLastServiceTime();
			int queueMin = 0;
			
			for(int i = 1; i < queueNumber; i++) {
				
				if(queues[i].getLastServiceTime() < min) {
					
					queueMin = i;
					min = queues[i].getLastServiceTime();
				}
			}
			
			queues[queueMin].addClientToQueue(waitingClient);
			queuesOver = false;
		}
	}
	

	public void run() {
		// TODO Auto-generated method stub
		
		for(int i = 0; i < queueNumber; i++) {
			
			queues[i] = new Queue(i, minimumIntervalOfArriving, maximumIntervalOfArriving,
					minimumServiceTime, maximumServiceTime, simulationInterval);
		}
		
		int ok = 0;
		
		while(!queuesOver || currentTime <= simulationInterval ) {
			
			if(!waitingClients.isEmpty())
				System.out.println("S: AT " + waitingClients.peekFirst().getArrivalTime() + " CT" + currentTime);
			
			Client c;
			
			if(!waitingClients.isEmpty()) {
				
				c = waitingClients.getFirst();
			
				while(c.getArrivalTime() <= currentTime && currentTime <= simulationInterval) {
					insertClient();
					if(!waitingClients.isEmpty())
						c = waitingClients.getFirst();
					else
						break;
				}
			}as.update(currentTime, getQueues());	
			//as.revalidate();		
			
			if(ok == 0) {
				for(int i = 0; i < queueNumber; i++) {
					
					Thread thread = new Thread(queues[i]);
					thread.start();
				}
				
				ok = 1;
			}
			
			for(int i = 0; i < queueNumber; i++) {
				
				if(maxWaitingTime < queues[i].getWatingTime()) {
					
					oraVarf = currentTime;
					maxWaitingTime = queues[i].getWatingTime();
				}
				
				mediumWaitingTime += queues[i].getWatingTime();
			}
			
			try {
		    	
				currentTime++;
				Queue.setCurrentTime(currentTime);
				Thread.sleep(unitateTimp);
				
			}catch(InterruptedException e){
				
				e.printStackTrace();
			}
			
			queuesOver = true;
			
			for(int i = 0; i < queueNumber; i++) {
				
				if(queues[i].getIsEmpty() == false) {
					
					queuesOver = false;
					break;
				}
			}
			
			if(!queuesOver)
				Queue.setCurrentTime(currentTime);
		}
		as.lastInfo(getRushHour(), getAverageWaitingTime(), getMaxWaitingTime());
	}

	private BlockingQueue<Client>[] getQueues(){
		
		BlockingQueue<Client>[] cozi =  new BlockingQueue[queueNumber];
		
		for(int i = 0; i < queueNumber; i++) {
			//System.out.println("DADADA " + i);
			cozi[i] = new LinkedBlockingQueue<Client>();
			cozi[i].addAll(queues[i].getClientsQueue());
		}
		
		return cozi;
	}
	
	public void setQueuesVisible() {
		
		as.setVisible(true);
	}
	
	public int getAverageWaitingTime() {
		
		return mediumWaitingTime / (simulationInterval * 3);
	}
	
	public int getMaxWaitingTime() {
		
		return maxWaitingTime;
	}
	
	public int getRushHour() {
		
		return oraVarf;
	}

}
