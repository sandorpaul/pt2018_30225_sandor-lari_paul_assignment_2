package PT2018.tema2.Tema2;

import java.util.*;

public class App{
    public static void main( String[] args )
    {
       
    	//GUI gui = new GUI();
    	//gui.setVisible(true);
    	
    	LinkedList<Client> c = new LinkedList<Client>();
    	c.add(new Client(1, 0, 1));
    	c.add(new Client(2, 0, 2));
    	c.add(new Client(3, 0, 4));
    	c.add(new Client(4, 0, 2));
    	c.add(new Client(5, 0, 3));
    	c.add(new Client(6, 1, 2));
    	c.add(new Client(7, 1, 2));
    	c.add(new Client(8, 1, 2));
    	c.add(new Client(9, 1, 2));
    	c.add(new Client(10, 1, 2));
    	c.add(new Client(11, 1, 2));
    	c.add(new Client(12, 11, 2));
    	
    	Scheduler s = new Scheduler(3, c, 1, 10, 1, 5, 15);
    	s.setQueuesVisible();
    	Thread t = new Thread(s);
    	
    	t.start();
   
    }

}
/*S: AT 0 CT0
BBBBBBBBB QUEUE 0 CLIENT1
BBBBBBBBB QUEUE 0 CLIENT4
BBBBBBBBB QUEUE 1 CLIENT2
BBBBBBBBB QUEUE 1 CLIENT5
BBBBBBBBB QUEUE 2 CLIENT3
Queue2: CT0Client3 LS4
Queue0: CT0Client1 LS3
Queue1: CT0Client2 LS5
Queue0: CT0Client4 LS3
Queue1: CT0Client5 LS5
Queue0: CT1Client4 LS3
S: AT 11 CT1
BBBBBBBBB QUEUE 1 CLIENT5*/