package PT2018.tema2.Tema2;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.*;

public class GUI extends JFrame implements ActionListener{

	private JLabel nrPersonsJL;
	private JLabel nrQueuesJL;
	private JTextField nrPersonsJTF;
	private JTextField nrQueuesJTF;
	private JLabel runningTimeJL;
	private JTextField runningTimeJTF;
	private JLabel minServiceTimeJL;
	private JTextField minServiceTimeJTF;
	private JLabel maxServiceTimeJL;
	private JTextField maxServiceTimeJTF;
	private JButton startJB;
	private Scheduler scheduler;
	
	GUI(){
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel.setPreferredSize(new Dimension(300, 220));
	
		JPanel p = new JPanel();
		
		nrPersonsJL = new JLabel("Numar total de persoane: ");
		nrQueuesJL = new JLabel("Numar de cozi: ");
		nrPersonsJTF = new JTextField(5);
		nrQueuesJTF = new JTextField(5);
		runningTimeJL = new JLabel("Timp de rulare: ");
		runningTimeJTF = new JTextField(5);
		minServiceTimeJL = new JLabel("Timp minim de servire: ");
		minServiceTimeJTF = new JTextField(5);
		maxServiceTimeJL = new JLabel("Timp maxim de servire: ");
		maxServiceTimeJTF = new JTextField(5);	
		startJB = new JButton("Start");
		
		p.add(nrPersonsJL);
		p.add(nrPersonsJTF);
		panel.add(p);
		//p.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		p = new JPanel();
		p.add(nrQueuesJL);
		p.add(nrQueuesJTF);
		panel.add(p);
		//p.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		p = new JPanel();
		p.add(runningTimeJL);
		p.add(runningTimeJTF);
		panel.add(p);
		
		p = new JPanel();
		p.add(minServiceTimeJL);
		p.add(minServiceTimeJTF);
		panel.add(p);
		
		p = new JPanel();
		p.add(maxServiceTimeJL);
		p.add(maxServiceTimeJTF);
		panel.add(p);

		panel.add(startJB);
		startJB.addActionListener(this);
		
		this.setContentPane(panel);
		this.pack(); 
		this.setTitle("Store"); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
		Object source = arg0.getSource();

		if(source == startJB) {
			
			Random rand = new Random();
			
			int nrClienti = Integer.parseInt(nrPersonsJTF.getText());
			
			LinkedList<Client> clienti = new LinkedList<Client>();
			
			Client c;
			
			for(int i = 0; i <  nrClienti; i++) {
				
				if( i == 0) 
					c = new Client(1, 0, rand.nextInt(Integer.parseInt(maxServiceTimeJTF.getText())) - 
							Integer.parseInt(minServiceTimeJTF.getText()) + 1);
				else {
					
					
					c = new Client(i + 1, clienti.peekLast().getArrivalTime() + rand.nextInt(2), rand.nextInt(Integer.parseInt(maxServiceTimeJTF.getText())) - 
							Integer.parseInt(minServiceTimeJTF.getText()) + 1);
			
				}
				
				
				clienti.add(c);
			}
			scheduler = new Scheduler(Integer.parseInt(nrQueuesJTF.getText()),clienti, 0, 15, Integer.parseInt(minServiceTimeJTF.getText()),
					Integer.parseInt(maxServiceTimeJTF.getText()), Integer.parseInt(runningTimeJTF.getText()));
			
			scheduler.setQueuesVisible();
			Thread t = new Thread(scheduler);
	    	
	    	t.start();
		}
	}
}
