package PT2018.tema2.Tema2;

import java.awt.Dimension;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.*;

public class AfisareSimulare extends JFrame{

	private JPanel statusPanel;
	private JScrollPane scrollFrame;
	
	private JPanel mainPanel;
	private JPanel[] queuesPanel;
	
	private LinkedList<JButton>[] clienti;
	private JLabel[] caseMarcat;
	private int nrCase;
	private BlockingQueue<Client>[] cozi; 
	private JLabel simulationTime;
	
	AfisareSimulare(int nrCase){
				
		statusPanel = new JPanel();
		statusPanel.setSize(new Dimension(1700, 70));
		scrollFrame = new JScrollPane(statusPanel);
		statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.Y_AXIS));

		scrollFrame.setPreferredSize(new Dimension( 800,300));
		statusPanel.setAutoscrolls(true);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
		simulationTime = new JLabel("Timp: 0");
		mainPanel.add(simulationTime);
		
		this.nrCase = nrCase;
		this.queuesPanel = new JPanel[nrCase];
		this.clienti = (LinkedList<JButton>[])new LinkedList[nrCase];
		this.cozi = (BlockingQueue<Client>[])new BlockingQueue[10];
		this.caseMarcat = new JLabel[nrCase];
		
		for(int i = 0; i < nrCase; i++) {
			
			this.queuesPanel[i] = new JPanel();
			this.caseMarcat[i] = new JLabel("Casa " + i);
			queuesPanel[i].add(caseMarcat[i]);
			this.clienti[i] = new LinkedList<JButton>();
			
			this.cozi[i] = new LinkedBlockingQueue<Client>();
			mainPanel.add(queuesPanel[i]);
		
		}
		
		mainPanel.add(scrollFrame);
		
		this.setContentPane(mainPanel);
		this.pack(); 
		this.setSize(new Dimension(1700, 270));
		this.setTitle("Cozi"); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void update(int simulationTime, BlockingQueue<Client>[] cozi) {
		
		//mainPanel = new JPanel();
		
		//mainPanel.add(this.simulationTime);
		
		this.simulationTime.setText("Timp: " + simulationTime);
		
		for(int i = 0; i < nrCase; i++) {
			
			queuesPanel[i].removeAll();
			queuesPanel[i].add(caseMarcat[i]);
			
			int j = 0;
			int queueSize = cozi[i].size();
			if(cozi[i] != null) {
			
				Client c;
				JButton button;
				
				for( ; j <= queueSize; j++) {
					
					c = cozi[i].poll();
					statusPanel.add(new JLabel("Current Time: " + simulationTime));
					if(c != null) {
					
						//System.out.println("DADADA " + cozi[i].size());
						button = new JButton("Client " + c.getClientId() + " " + 
								"ArrivalTime: " + c.getArrivalTime() + " EndTime: " + c.getEndServiceTime());
						button.setPreferredSize(new Dimension(250, 36));
						clienti[i].add(button);
						statusPanel.add(new JLabel("Casa " + i + ": Client " + c.getClientId() + " ArrivalTime: " + c.getArrivalTime() +
								 " ServiceTime: " + c.getStartServingTime() + " Leaves: " + c.getEndServiceTime()));
					}
					
					else break;
				}
			}
			//for( ; j < 5; j++)
				//clienti[i].add(new JButton("0\\n0\\n0\\n0"));

			for(j = 0; j < queueSize; j++) {
				queuesPanel[i].add(clienti[i].poll());
			}
		}
		
		revalidate();
	}
	
	public void lastInfo(int rushHour, int averageWaitingTime, int maxWaitingTime) {
		
		statusPanel.add(new JLabel("Average Waiting Time: " + averageWaitingTime));
		statusPanel.add(new JLabel("Maximum Waiting Time: " + maxWaitingTime));
		statusPanel.add(new JLabel("Rush Hour: " + rushHour));
		
		revalidate();
	}
	
}
